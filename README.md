# README #

Listing interesting article links for future reference.

## Articles ##

* [Google MobileNets](https://research.googleblog.com/2017/06/mobilenets-open-source-models-for.html?utm_content=56007852&utm_medium=social&utm_source=linkedin)
* [Microsoft Research aims for smart edge for AI](https://blogs.microsoft.com/next/2017/06/29/ais-big-leap-tiny-devices-opens-world-possibilities/)
* [Embedded deep learning workshop](http://www.cs.ucl.ac.uk/deepmobile_wkshp/program.html)
* [Self normalizing neural networks](https://medium.com/@damoncivin/self-normalising-neural-networks-snn-2a972c1d421)
* [Paper: Acoustic Modeling for Google Home](https://research.google.com/pubs/pub46130.html)
* [Wavenet architecture](https://www.nextplatform.com/2017/08/23/first-depth-view-wave-computings-dpu-architecture-systems/)
* [Brainchip SNN accelerator](http://www.brainchipinc.com/technology)
* [Apple ML Framework](https://developer.apple.com/machine-learning/)
* [Evolving Stable Strategies](http://blog.otoro.net/2017/11/12/evolving-stable-strategies/)
* [Flexpoint: Intel Nervana](https://ai.intel.com/flexpoint-numerical-innovation-underlying-intel-nervana-neural-network-processor/)
* [Nervana Architecture](https://www.nextplatform.com/2018/01/11/intel-nervana-shed-light-deep-learning-chip-architecture/)

### AutoML ###
* [Cloud AutoML blog] (https://blog.google/topics/google-cloud/cloud-automl-making-ai-accessible-every-business/)
* [Cloud AutoML access page](https://cloud.google.com/automl/)
### GPGPU on RPI ###
* [GPGPU on Raspberry Pi](https://www.raspberrypi.org/blog/gpgpu-hacking-on-the-pi/)
* [QPU Examples](https://github.com/mn416/QPULib)

## Repositories ##
* [Microsoft Embedded Learning Library](https://github.com/Microsoft/ELL)
* [List of neural processing hardware](https://github.com/basicmi/Deep-Learning-Processor-List?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_recent_activity_details_all%3BENTEvYERRpSn4VxckVLn9g%3D%3D)
* [DNN Mark](https://github.com/doody1986/DNNMark)

## Video / Lectures ##
* [Graphcore](https://www.youtube.com/watch?v=Gh-Tff7DdzU)